## Supervised learning and OCR of Hand-written Data using SVM

OCR of Hand-written Data using SVM based on [THIS](https://docs.opencv.org/4.x/dd/d3b/tutorial_py_svm_opencv.html) tutorial.
